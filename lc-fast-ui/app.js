const Koa = require('koa');
const path = require('path')
const static = require('koa-static')
var route = require('koa-route');


const app = new Koa();

//设置静态资源的路径 
const staticPath = './public'
 
app.use(static(
  path.join( __dirname,  staticPath)
))



// log request URL:
app.use(async (ctx, next) => {
    console.log(`Process ${ctx.request.method} ${ctx.request.url}...`);
    await next();
});

app.listen(3000);
console.log('app started at port 3000...');