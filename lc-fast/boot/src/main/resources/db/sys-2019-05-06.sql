/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 50725
 Source Host           : localhost:3306
 Source Schema         : test

 Target Server Type    : MySQL
 Target Server Version : 50725
 File Encoding         : 65001

 Date: 06/05/2019 20:47:55
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `parent_id` bigint(20) NULL DEFAULT 0 COMMENT '父菜单ID，一级菜单为0',
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '菜单名称',
  `url` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '菜单URL',
  `perms` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '授权(多个用逗号分隔，如：user:list,user:create)',
  `type` int(11) NULL DEFAULT NULL COMMENT '类型     0：目录  1：菜单   2：按钮   ',
  `icon` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '菜单图标',
  `order_num` int(11) NULL DEFAULT NULL COMMENT '排序',
  `create_time` timestamp(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `update_time` timestamp(0) NULL DEFAULT NULL COMMENT '更新时间',
  `deleted` tinyint(1) NOT NULL DEFAULT 0 COMMENT '是否删除 1是 0否',
  `version` int(11) NOT NULL DEFAULT 1 COMMENT '版本号',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 181 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '菜单管理' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
INSERT INTO `sys_menu` VALUES (1, 0, '基础管理', '', '', 0, 'fa fa-bars', 0, '2019-05-06 15:17:51', NULL, 0, 1);
INSERT INTO `sys_menu` VALUES (2, 3, '系统菜单', 'sys/menu/', '', 1, 'fa fa-th-list', 2, '2019-05-06 15:18:38', NULL, 0, 1);
INSERT INTO `sys_menu` VALUES (3, 0, '系统管理', NULL, NULL, 0, 'fa fa-desktop', 1, '2019-05-06 15:17:54', NULL, 0, 1);
INSERT INTO `sys_menu` VALUES (6, 3, '用户管理', 'sys/user/', '', 1, 'fa fa-user', 0, '2019-05-06 15:18:40', NULL, 0, 1);
INSERT INTO `sys_menu` VALUES (7, 3, '角色管理', 'sys/role', '', 1, 'fa fa-paw', 1, '2019-05-06 15:18:41', NULL, 0, 1);

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role`  (
  `id` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `role_name` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '角色名称',
  `role_sign` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '角色标识',
  `remark` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  `create_user_id` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `create_time` timestamp(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `update_time` timestamp(0) NULL DEFAULT NULL COMMENT '更新时间',
  `deleted` tinyint(1) NOT NULL DEFAULT 0 COMMENT '是否删除 1是 0否',
  `version` int(11) NOT NULL DEFAULT 1 COMMENT '版本号',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '角色' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES ('admin', '超级用户角色', 'admin', '超级管理员', NULL, '2019-05-06 15:17:44', '2019-05-06 15:17:42', 0, 1);

-- ----------------------------
-- Table structure for sys_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_menu`;
CREATE TABLE `sys_role_menu`  (
  `id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `role_id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '角色ID',
  `menu_id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '菜单ID',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '角色与菜单对应关系' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_role_menu
-- ----------------------------
INSERT INTO `sys_role_menu` VALUES ('1064', '54', '53');
INSERT INTO `sys_role_menu` VALUES ('1095', '55', '2');
INSERT INTO `sys_role_menu` VALUES ('1096', '55', '6');
INSERT INTO `sys_role_menu` VALUES ('1097', '55', '7');
INSERT INTO `sys_role_menu` VALUES ('1098', '55', '3');
INSERT INTO `sys_role_menu` VALUES ('1099', '55', '50');
INSERT INTO `sys_role_menu` VALUES ('1100', '55', '49');
INSERT INTO `sys_role_menu` VALUES ('1101', '55', '1');
INSERT INTO `sys_role_menu` VALUES ('1856', '53', '28');
INSERT INTO `sys_role_menu` VALUES ('1857', '53', '29');
INSERT INTO `sys_role_menu` VALUES ('1858', '53', '30');
INSERT INTO `sys_role_menu` VALUES ('1859', '53', '27');
INSERT INTO `sys_role_menu` VALUES ('1860', '53', '57');
INSERT INTO `sys_role_menu` VALUES ('1861', '53', '71');
INSERT INTO `sys_role_menu` VALUES ('1862', '53', '48');
INSERT INTO `sys_role_menu` VALUES ('1863', '53', '72');
INSERT INTO `sys_role_menu` VALUES ('1864', '53', '1');
INSERT INTO `sys_role_menu` VALUES ('1865', '53', '7');
INSERT INTO `sys_role_menu` VALUES ('1866', '53', '55');
INSERT INTO `sys_role_menu` VALUES ('1867', '53', '56');
INSERT INTO `sys_role_menu` VALUES ('1868', '53', '62');
INSERT INTO `sys_role_menu` VALUES ('1869', '53', '15');
INSERT INTO `sys_role_menu` VALUES ('1870', '53', '2');
INSERT INTO `sys_role_menu` VALUES ('1871', '53', '61');
INSERT INTO `sys_role_menu` VALUES ('1872', '53', '20');
INSERT INTO `sys_role_menu` VALUES ('1873', '53', '21');
INSERT INTO `sys_role_menu` VALUES ('1874', '53', '22');
INSERT INTO `sys_role_menu` VALUES ('2247', '63', '-1');
INSERT INTO `sys_role_menu` VALUES ('2248', '63', '84');
INSERT INTO `sys_role_menu` VALUES ('2249', '63', '85');
INSERT INTO `sys_role_menu` VALUES ('2250', '63', '88');
INSERT INTO `sys_role_menu` VALUES ('2251', '63', '87');
INSERT INTO `sys_role_menu` VALUES ('2252', '64', '84');
INSERT INTO `sys_role_menu` VALUES ('2253', '64', '89');
INSERT INTO `sys_role_menu` VALUES ('2254', '64', '88');
INSERT INTO `sys_role_menu` VALUES ('2255', '64', '87');
INSERT INTO `sys_role_menu` VALUES ('2256', '64', '86');
INSERT INTO `sys_role_menu` VALUES ('2257', '64', '85');
INSERT INTO `sys_role_menu` VALUES ('2258', '65', '89');
INSERT INTO `sys_role_menu` VALUES ('2259', '65', '88');
INSERT INTO `sys_role_menu` VALUES ('2260', '65', '86');
INSERT INTO `sys_role_menu` VALUES ('2262', '67', '48');
INSERT INTO `sys_role_menu` VALUES ('2263', '68', '88');
INSERT INTO `sys_role_menu` VALUES ('2264', '68', '87');
INSERT INTO `sys_role_menu` VALUES ('2265', '69', '89');
INSERT INTO `sys_role_menu` VALUES ('2266', '69', '88');
INSERT INTO `sys_role_menu` VALUES ('2267', '69', '86');
INSERT INTO `sys_role_menu` VALUES ('2268', '69', '87');
INSERT INTO `sys_role_menu` VALUES ('2269', '69', '85');
INSERT INTO `sys_role_menu` VALUES ('2270', '69', '84');
INSERT INTO `sys_role_menu` VALUES ('2271', '70', '85');
INSERT INTO `sys_role_menu` VALUES ('2272', '70', '89');
INSERT INTO `sys_role_menu` VALUES ('2273', '70', '88');
INSERT INTO `sys_role_menu` VALUES ('2274', '70', '87');
INSERT INTO `sys_role_menu` VALUES ('2275', '70', '86');
INSERT INTO `sys_role_menu` VALUES ('2276', '70', '84');
INSERT INTO `sys_role_menu` VALUES ('2277', '71', '87');
INSERT INTO `sys_role_menu` VALUES ('2278', '72', '59');
INSERT INTO `sys_role_menu` VALUES ('2279', '73', '48');
INSERT INTO `sys_role_menu` VALUES ('2280', '74', '88');
INSERT INTO `sys_role_menu` VALUES ('2281', '74', '87');
INSERT INTO `sys_role_menu` VALUES ('2282', '75', '88');
INSERT INTO `sys_role_menu` VALUES ('2283', '75', '87');
INSERT INTO `sys_role_menu` VALUES ('2284', '76', '85');
INSERT INTO `sys_role_menu` VALUES ('2285', '76', '89');
INSERT INTO `sys_role_menu` VALUES ('2286', '76', '88');
INSERT INTO `sys_role_menu` VALUES ('2287', '76', '87');
INSERT INTO `sys_role_menu` VALUES ('2288', '76', '86');
INSERT INTO `sys_role_menu` VALUES ('2289', '76', '84');
INSERT INTO `sys_role_menu` VALUES ('2292', '78', '88');
INSERT INTO `sys_role_menu` VALUES ('2293', '78', '87');
INSERT INTO `sys_role_menu` VALUES ('2294', '78', NULL);
INSERT INTO `sys_role_menu` VALUES ('2295', '78', NULL);
INSERT INTO `sys_role_menu` VALUES ('2296', '78', NULL);
INSERT INTO `sys_role_menu` VALUES ('2308', '80', '87');
INSERT INTO `sys_role_menu` VALUES ('2309', '80', '86');
INSERT INTO `sys_role_menu` VALUES ('2310', '80', '-1');
INSERT INTO `sys_role_menu` VALUES ('2311', '80', '84');
INSERT INTO `sys_role_menu` VALUES ('2312', '80', '85');
INSERT INTO `sys_role_menu` VALUES ('2328', '79', '72');
INSERT INTO `sys_role_menu` VALUES ('2329', '79', '48');
INSERT INTO `sys_role_menu` VALUES ('2330', '79', '77');
INSERT INTO `sys_role_menu` VALUES ('2331', '79', '84');
INSERT INTO `sys_role_menu` VALUES ('2332', '79', '89');
INSERT INTO `sys_role_menu` VALUES ('2333', '79', '88');
INSERT INTO `sys_role_menu` VALUES ('2334', '79', '87');
INSERT INTO `sys_role_menu` VALUES ('2335', '79', '86');
INSERT INTO `sys_role_menu` VALUES ('2336', '79', '85');
INSERT INTO `sys_role_menu` VALUES ('2337', '79', '-1');
INSERT INTO `sys_role_menu` VALUES ('2338', '77', '89');
INSERT INTO `sys_role_menu` VALUES ('2339', '77', '88');
INSERT INTO `sys_role_menu` VALUES ('2340', '77', '87');
INSERT INTO `sys_role_menu` VALUES ('2341', '77', '86');
INSERT INTO `sys_role_menu` VALUES ('2342', '77', '85');
INSERT INTO `sys_role_menu` VALUES ('2343', '77', '84');
INSERT INTO `sys_role_menu` VALUES ('2344', '77', '72');
INSERT INTO `sys_role_menu` VALUES ('2345', '77', '-1');
INSERT INTO `sys_role_menu` VALUES ('2346', '77', '77');
INSERT INTO `sys_role_menu` VALUES ('3178', '56', '68');
INSERT INTO `sys_role_menu` VALUES ('3179', '56', '60');
INSERT INTO `sys_role_menu` VALUES ('3180', '56', '59');
INSERT INTO `sys_role_menu` VALUES ('3181', '56', '58');
INSERT INTO `sys_role_menu` VALUES ('3182', '56', '51');
INSERT INTO `sys_role_menu` VALUES ('3183', '56', '50');
INSERT INTO `sys_role_menu` VALUES ('3184', '56', '49');
INSERT INTO `sys_role_menu` VALUES ('3185', '56', '-1');
INSERT INTO `sys_role_menu` VALUES ('367', '44', '1');
INSERT INTO `sys_role_menu` VALUES ('368', '44', '32');
INSERT INTO `sys_role_menu` VALUES ('369', '44', '33');
INSERT INTO `sys_role_menu` VALUES ('370', '44', '34');
INSERT INTO `sys_role_menu` VALUES ('371', '44', '35');
INSERT INTO `sys_role_menu` VALUES ('372', '44', '28');
INSERT INTO `sys_role_menu` VALUES ('373', '44', '29');
INSERT INTO `sys_role_menu` VALUES ('374', '44', '30');
INSERT INTO `sys_role_menu` VALUES ('375', '44', '38');
INSERT INTO `sys_role_menu` VALUES ('376', '44', '4');
INSERT INTO `sys_role_menu` VALUES ('377', '44', '27');
INSERT INTO `sys_role_menu` VALUES ('378', '45', '38');
INSERT INTO `sys_role_menu` VALUES ('379', '46', '3');
INSERT INTO `sys_role_menu` VALUES ('380', '46', '20');
INSERT INTO `sys_role_menu` VALUES ('381', '46', '21');
INSERT INTO `sys_role_menu` VALUES ('382', '46', '22');
INSERT INTO `sys_role_menu` VALUES ('383', '46', '23');
INSERT INTO `sys_role_menu` VALUES ('384', '46', '11');
INSERT INTO `sys_role_menu` VALUES ('385', '46', '12');
INSERT INTO `sys_role_menu` VALUES ('386', '46', '13');
INSERT INTO `sys_role_menu` VALUES ('387', '46', '14');
INSERT INTO `sys_role_menu` VALUES ('388', '46', '24');
INSERT INTO `sys_role_menu` VALUES ('389', '46', '25');
INSERT INTO `sys_role_menu` VALUES ('390', '46', '26');
INSERT INTO `sys_role_menu` VALUES ('391', '46', '15');
INSERT INTO `sys_role_menu` VALUES ('392', '46', '2');
INSERT INTO `sys_role_menu` VALUES ('393', '46', '6');
INSERT INTO `sys_role_menu` VALUES ('394', '46', '7');
INSERT INTO `sys_role_menu` VALUES ('4146', '57', '79');
INSERT INTO `sys_role_menu` VALUES ('4147', '57', '80');
INSERT INTO `sys_role_menu` VALUES ('4148', '57', '152');
INSERT INTO `sys_role_menu` VALUES ('4149', '57', '153');
INSERT INTO `sys_role_menu` VALUES ('4150', '57', '154');
INSERT INTO `sys_role_menu` VALUES ('4151', '57', '155');
INSERT INTO `sys_role_menu` VALUES ('4152', '57', '156');
INSERT INTO `sys_role_menu` VALUES ('4153', '57', '158');
INSERT INTO `sys_role_menu` VALUES ('4154', '57', '159');
INSERT INTO `sys_role_menu` VALUES ('4155', '57', '160');
INSERT INTO `sys_role_menu` VALUES ('4156', '57', '161');
INSERT INTO `sys_role_menu` VALUES ('4157', '57', '162');
INSERT INTO `sys_role_menu` VALUES ('4158', '57', '164');
INSERT INTO `sys_role_menu` VALUES ('4159', '57', '165');
INSERT INTO `sys_role_menu` VALUES ('4160', '57', '166');
INSERT INTO `sys_role_menu` VALUES ('4161', '57', '167');
INSERT INTO `sys_role_menu` VALUES ('4162', '57', '168');
INSERT INTO `sys_role_menu` VALUES ('4163', '57', '170');
INSERT INTO `sys_role_menu` VALUES ('4164', '57', '171');
INSERT INTO `sys_role_menu` VALUES ('4165', '57', '172');
INSERT INTO `sys_role_menu` VALUES ('4166', '57', '173');
INSERT INTO `sys_role_menu` VALUES ('4167', '57', '174');
INSERT INTO `sys_role_menu` VALUES ('4168', '57', '28');
INSERT INTO `sys_role_menu` VALUES ('4169', '57', '29');
INSERT INTO `sys_role_menu` VALUES ('4170', '57', '30');
INSERT INTO `sys_role_menu` VALUES ('4171', '57', '57');
INSERT INTO `sys_role_menu` VALUES ('4172', '57', '92');
INSERT INTO `sys_role_menu` VALUES ('4173', '57', '151');
INSERT INTO `sys_role_menu` VALUES ('4174', '57', '157');
INSERT INTO `sys_role_menu` VALUES ('4175', '57', '163');
INSERT INTO `sys_role_menu` VALUES ('4176', '57', '169');
INSERT INTO `sys_role_menu` VALUES ('4177', '57', '27');
INSERT INTO `sys_role_menu` VALUES ('4178', '57', '91');
INSERT INTO `sys_role_menu` VALUES ('4179', '57', '-1');
INSERT INTO `sys_role_menu` VALUES ('4180', '57', '1');
INSERT INTO `sys_role_menu` VALUES ('4181', '57', '78');
INSERT INTO `sys_role_menu` VALUES ('4262', '1', '71');
INSERT INTO `sys_role_menu` VALUES ('4263', '1', '79');
INSERT INTO `sys_role_menu` VALUES ('4264', '1', '80');
INSERT INTO `sys_role_menu` VALUES ('4265', '1', '81');
INSERT INTO `sys_role_menu` VALUES ('4266', '1', '83');
INSERT INTO `sys_role_menu` VALUES ('4267', '1', '20');
INSERT INTO `sys_role_menu` VALUES ('4268', '1', '21');
INSERT INTO `sys_role_menu` VALUES ('4269', '1', '22');
INSERT INTO `sys_role_menu` VALUES ('4270', '1', '61');
INSERT INTO `sys_role_menu` VALUES ('4271', '1', '12');
INSERT INTO `sys_role_menu` VALUES ('4272', '1', '13');
INSERT INTO `sys_role_menu` VALUES ('4273', '1', '14');
INSERT INTO `sys_role_menu` VALUES ('4274', '1', '24');
INSERT INTO `sys_role_menu` VALUES ('4275', '1', '25');
INSERT INTO `sys_role_menu` VALUES ('4276', '1', '26');
INSERT INTO `sys_role_menu` VALUES ('4277', '1', '15');
INSERT INTO `sys_role_menu` VALUES ('4278', '1', '55');
INSERT INTO `sys_role_menu` VALUES ('4279', '1', '56');
INSERT INTO `sys_role_menu` VALUES ('4280', '1', '62');
INSERT INTO `sys_role_menu` VALUES ('4281', '1', '74');
INSERT INTO `sys_role_menu` VALUES ('4282', '1', '75');
INSERT INTO `sys_role_menu` VALUES ('4283', '1', '76');
INSERT INTO `sys_role_menu` VALUES ('4284', '1', '48');
INSERT INTO `sys_role_menu` VALUES ('4285', '1', '72');
INSERT INTO `sys_role_menu` VALUES ('4286', '1', '28');
INSERT INTO `sys_role_menu` VALUES ('4287', '1', '29');
INSERT INTO `sys_role_menu` VALUES ('4288', '1', '30');
INSERT INTO `sys_role_menu` VALUES ('4289', '1', '57');
INSERT INTO `sys_role_menu` VALUES ('4290', '1', '92');
INSERT INTO `sys_role_menu` VALUES ('4291', '1', '78');
INSERT INTO `sys_role_menu` VALUES ('4292', '1', '2');
INSERT INTO `sys_role_menu` VALUES ('4293', '1', '6');
INSERT INTO `sys_role_menu` VALUES ('4294', '1', '7');
INSERT INTO `sys_role_menu` VALUES ('4295', '1', '73');
INSERT INTO `sys_role_menu` VALUES ('4296', '1', '3');
INSERT INTO `sys_role_menu` VALUES ('4297', '1', '77');
INSERT INTO `sys_role_menu` VALUES ('4298', '1', '27');
INSERT INTO `sys_role_menu` VALUES ('4299', '1', '91');
INSERT INTO `sys_role_menu` VALUES ('4300', '1', '175');
INSERT INTO `sys_role_menu` VALUES ('4301', '1', '176');
INSERT INTO `sys_role_menu` VALUES ('4302', '1', '177');
INSERT INTO `sys_role_menu` VALUES ('4303', '1', '178');
INSERT INTO `sys_role_menu` VALUES ('4304', '1', '179');
INSERT INTO `sys_role_menu` VALUES ('4305', '1', '180');
INSERT INTO `sys_role_menu` VALUES ('4306', '1', '-1');
INSERT INTO `sys_role_menu` VALUES ('4307', '1', '1');
INSERT INTO `sys_role_menu` VALUES ('598', '50', '38');
INSERT INTO `sys_role_menu` VALUES ('632', '38', '42');
INSERT INTO `sys_role_menu` VALUES ('737', '51', '38');
INSERT INTO `sys_role_menu` VALUES ('738', '51', '39');
INSERT INTO `sys_role_menu` VALUES ('739', '51', '40');
INSERT INTO `sys_role_menu` VALUES ('740', '51', '41');
INSERT INTO `sys_role_menu` VALUES ('741', '51', '4');
INSERT INTO `sys_role_menu` VALUES ('742', '51', '32');
INSERT INTO `sys_role_menu` VALUES ('743', '51', '33');
INSERT INTO `sys_role_menu` VALUES ('744', '51', '34');
INSERT INTO `sys_role_menu` VALUES ('745', '51', '35');
INSERT INTO `sys_role_menu` VALUES ('746', '51', '27');
INSERT INTO `sys_role_menu` VALUES ('747', '51', '28');
INSERT INTO `sys_role_menu` VALUES ('748', '51', '29');
INSERT INTO `sys_role_menu` VALUES ('749', '51', '30');
INSERT INTO `sys_role_menu` VALUES ('750', '51', '1');

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user`  (
  `id` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '主键 uuid',
  `username` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '用户名',
  `password` varchar(80) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '密码',
  `realname` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '真实姓名',
  `sex` int(1) NOT NULL DEFAULT 1 COMMENT '性别 1男 2女',
  `dept_id` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '部门id',
  `email` varchar(80) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'email',
  `mobile` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '手机号',
  `status` int(1) NOT NULL DEFAULT 1 COMMENT '状态 1启用 2禁用',
  `birth` timestamp(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '生日',
  `hobby` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '爱好',
  `province` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '省',
  `city` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '市',
  `district` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '县',
  `live_address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '居住地址',
  `create_user_id` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `create_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `update_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  `deleted` tinyint(1) NOT NULL DEFAULT 0 COMMENT '是否删除 1是 0否',
  `version` int(11) NOT NULL DEFAULT 1 COMMENT '版本号',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `username`(`username`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '用户表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES ('01f806e0b92142bb89f4b7691c23aa13', 'admin2', 'admin2', 'admin', 2, NULL, '575873200@qq.com', '18511893521', 1, '2019-05-06 15:07:16', NULL, NULL, NULL, NULL, NULL, NULL, '2019-05-06 15:07:16', '2018-09-18 02:13:56', 0, 47);
INSERT INTO `sys_user` VALUES ('0604eaf381834ac0b1d5325a6a3ec5f5', 'admin3', 'admin3', 'admin', 2, NULL, '4123231@qq.com', '12312312312', 1, '2019-05-06 15:07:13', NULL, NULL, NULL, NULL, NULL, NULL, '2019-05-06 15:07:13', '2018-09-18 02:13:55', 0, 16);
INSERT INTO `sys_user` VALUES ('1340522be0191b0cd6968c97c77d8e76', 'yuyanan47598', NULL, NULL, 1, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-05-06 09:37:20', '2019-05-06 17:37:19', 0, 1);
INSERT INTO `sys_user` VALUES ('3e5cef60bf984360a5b723571192ce3d', 'fdsafsda123', 'fdsafsda123', 'fdsafsda123', 2, NULL, 'fdsafsda123@qq.com', '12332123123', 1, '2019-05-06 17:23:11', NULL, NULL, NULL, NULL, NULL, NULL, '2019-05-06 17:23:11', '2019-05-06 15:07:10', 0, 1);
INSERT INTO `sys_user` VALUES ('667703b43f75de098971efca186675a5', 'yuyanan22533', NULL, NULL, 1, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-05-06 09:36:04', '2019-05-06 17:36:04', 0, 1);
INSERT INTO `sys_user` VALUES ('810d95dbca0d4d758463c8a963b0840f', 'admin6', 'admin6', '444', 2, NULL, '1231@qq.com', '12312311231', 1, '2019-05-06 15:07:11', NULL, NULL, NULL, NULL, NULL, NULL, '2019-05-06 15:07:11', '2018-09-18 00:54:51', 0, 4);
INSERT INTO `sys_user` VALUES ('997aceb476795d0b3ac3f7eec3cf8587', 'yuyanan1', NULL, NULL, 1, NULL, NULL, NULL, 1, '2019-05-06 17:23:10', NULL, NULL, NULL, NULL, NULL, NULL, '2019-05-06 15:07:10', '2019-05-06 15:07:10', 0, 1);
INSERT INTO `sys_user` VALUES ('adee4f4fed1d4441a506a8fd1c313b1c', 'eeeee123', 'eeeee123', 'admineeeee123', 2, NULL, 'eeeee123@qq.com', '12312312321', 1, '2019-05-06 17:23:12', NULL, NULL, NULL, NULL, NULL, NULL, '2019-05-06 17:23:12', '2019-05-06 15:07:10', 0, 1);
INSERT INTO `sys_user` VALUES ('admin', 'admin', 'admin', 'test', 2, NULL, NULL, NULL, 1, '2019-05-06 15:07:11', NULL, NULL, NULL, NULL, NULL, NULL, '2019-05-06 15:07:11', '2018-07-16 17:42:38', 0, 15);
INSERT INTO `sys_user` VALUES ('d658e0b949a845149cfb8098709ea12b', 'string', 'string', 'string', 1, NULL, 'string', 'string', 1, '2019-05-06 17:23:14', NULL, NULL, NULL, NULL, NULL, NULL, '2019-05-06 17:23:14', '2019-05-06 15:07:10', 0, 1);

-- ----------------------------
-- Table structure for sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role`  (
  `id` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `user_id` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '用户ID',
  `role_id` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '角色ID',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `wfk_sys_user_role_user_id`(`user_id`) USING BTREE,
  INDEX `wfk_sys_user_role_role_id`(`role_id`) USING BTREE,
  CONSTRAINT `wfk_sys_user_role_role_id` FOREIGN KEY (`role_id`) REFERENCES `sys_role` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `wfk_sys_user_role_user_id` FOREIGN KEY (`user_id`) REFERENCES `sys_user` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户与角色对应关系' ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;
