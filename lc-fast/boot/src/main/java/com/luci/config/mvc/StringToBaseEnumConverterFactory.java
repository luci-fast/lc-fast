/**  
 * All rights Reserved, Designed By www.tydic.com
 * @Title:  StringToBaseEnumConverterFactory.java   
 * @Package com.fast.admin.config   
 * @Description:   
 * @author: yuyanan  
 * @date:   2018年9月15日   
 * @version V1.0 
 * @Copyright:  yuyanan
 * 
 */
package com.luci.config.mvc;

import com.luci.common.supers.IBindEnum;
import org.springframework.core.convert.converter.Converter;
import org.springframework.core.convert.converter.ConverterFactory;

/**    
 * 解决mvc String转换为枚举的问题
 * @author: yuyanan
 * @date:   2018年9月15日      
 */
public class StringToBaseEnumConverterFactory implements ConverterFactory<String, IBindEnum> {

	@Override
	public <T extends IBindEnum> Converter<String, T> getConverter(
			Class<T> targetType) {
		if (!targetType.isEnum()) {
            throw new UnsupportedOperationException("只支持转换到枚举类型");
        }
        return new StringToBaseEnumConverter(targetType);
	}

	 private class StringToBaseEnumConverter<T extends IBindEnum> implements Converter<String, T> {
	        private final Class<T> enumType;

	        public StringToBaseEnumConverter(Class<T> enumType) {
	            this.enumType = enumType;
	        }

	        @Override
	        public T convert(String s) {
	            for (T t : enumType.getEnumConstants()) {
	                if (s.equals(t.getKey())) {
	                    return t;
	                }
	            }
	            return null;
	        }
	    }
	

}
