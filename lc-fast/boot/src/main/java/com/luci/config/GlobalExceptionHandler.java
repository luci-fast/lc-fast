package com.luci.config;

import com.baomidou.mybatisplus.extension.api.IErrorCode;
import com.baomidou.mybatisplus.extension.api.R;
import com.baomidou.mybatisplus.extension.enums.ApiErrorCode;
import com.baomidou.mybatisplus.extension.exceptions.ApiException;
import com.luci.common.exception.ApiCode;
import com.luci.common.tool.JsonTool;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 通用 Api Controller 全局异常处理
 * </p>
 */
@RestControllerAdvice
public class GlobalExceptionHandler {
	private static final Logger logger = LoggerFactory
			.getLogger(GlobalExceptionHandler.class);

	/**
	 * <p>
	 * 自定义 REST 业务异常
	 * <p>
	 *
	 * @param e
	 *            异常类型
	 * @return
	 */
	@ExceptionHandler(value = Exception.class)
	public R handleBadRequest(Exception e) {
		e.printStackTrace();
		/*
		 * 业务逻辑警告提示
		 */
		if (e instanceof ApiException) { //TODO
			IErrorCode errorCode = ((ApiException) e).getErrorCode();
			logger.error("Rest request error, {}", e.getMessage());
			if (null != errorCode) {
				R r = new R(errorCode);
				r.setCode(errorCode.getCode());
				r.setMsg(errorCode.getMsg());
				return r;
			}
			R r = new R(ApiErrorCode.FAILED);
			r.setMsg(e.getMessage());
			return r;
		}

		/*
		 * 参数校验异常
		 */
		if (e instanceof BindException) {
			BindingResult bindingResult = ((BindException) e)
					.getBindingResult();
			if (null != bindingResult && bindingResult.hasErrors()) {
				List<Object> jsonList = new ArrayList<>();
				bindingResult.getFieldErrors().stream().forEach(fieldError -> {
					Map<String, Object> jsonObject = new HashMap<>(2);
					jsonObject.put("name", fieldError.getField());
					jsonObject.put("msg", fieldError.getDefaultMessage());
					jsonList.add(jsonObject);
				});
				R r = new R();
				r.setCode(ApiErrorCode.FAILED.getCode());
				r.setMsg(JsonTool.beanToJson(jsonList));
				return r;
			}
		}
		
		/*
		 * 数据关联删除异常
		 */
		if (e instanceof DataIntegrityViolationException) {
			R r = new R();
			r.setCode(ApiErrorCode.FAILED.getCode());
			r.setMsg("数据被使用，不能删除");
			return r;
		}


		/**
		 * 系统内部异常，打印异常栈
		 */
		logger.error("Error: System Exception", e);
		return new R(ApiCode.ERROR);
	}
}
