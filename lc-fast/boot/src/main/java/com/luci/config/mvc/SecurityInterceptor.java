/**
 * All rights Reserved, Designed By www.tydic.com
 *
 * @Title: SecurityInterceptor.java
 * @Package com.fast.admin.config
 * @Description:
 * @author: yuyanan
 * @date: 2018年9月13日
 * @version V1.0
 * @Copyright: yuyanan
 */
package com.luci.config.mvc;

import com.baomidou.mybatisplus.extension.api.R;
import com.baomidou.mybatisplus.extension.enums.ApiErrorCode;
import com.luci.base.SuperController;
import com.luci.common.exception.ApiCode;
import com.luci.common.tool.JsonTool;
import com.luci.sys.entity.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * 登录拦截器
 * 
 * @author: yuyanan
 * @date: 2018年9月13日
 */
@Slf4j
public class SecurityInterceptor extends HandlerInterceptorAdapter {

	@Override
	public boolean preHandle(HttpServletRequest request,
                             HttpServletResponse response, Object handler) throws Exception {
		log.info("==============登录验证================");
		String requestUri = request.getRequestURI();
		String contextPath = request.getContextPath();
		String url = requestUri.substring(contextPath.length());

		log.info("requestUri:" + requestUri);
		log.info("contextPath:" + contextPath);
		log.info("url:" + url);

		HttpSession session = request.getSession(false);
		if (session == null) {
			return invalidLogin(response);
		}

		User user = (User) session
				.getAttribute(SuperController.SESSION_USER);
		if (user == null) {
			return invalidLogin(response);
		} else {
			return true;
		}

	}

	private boolean invalidLogin(HttpServletResponse response)
			throws IOException {
		log.info("Interceptor：登录超时");
		response.setCharacterEncoding("UTF-8");
		response.setContentType("application/json");
		response.getWriter().write(
				JsonTool.beanToJson(R.failed(ApiCode.LOGIN_INVALID)));
		return false;
	}
}
