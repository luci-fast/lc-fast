package com.luci.sys.model;

import com.luci.common.supers.SuperPageBo;
import io.swagger.annotations.ApiModel;
import lombok.Data;

/**
 * <p>
 * 用户表 分页参数
 * </p>
 *
 * @author yuyanan
 * @since 2019-05-06
 */
@ApiModel("用户表分页查询对象")
@Data
public class UserPageBo extends SuperPageBo {

    private String username;

    private String realname;

    private String mobile;

}
