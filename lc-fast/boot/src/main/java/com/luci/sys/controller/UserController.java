package com.luci.sys.controller;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.api.Assert;
import com.baomidou.mybatisplus.extension.api.R;
import com.luci.base.SuperController;
import com.luci.common.exception.ApiCode;
import com.luci.sys.entity.User;
import com.luci.sys.model.UserAddBo;
import com.luci.sys.model.UserEditBo;
import com.luci.sys.model.UserPageBo;
import com.luci.sys.service.IUserService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 * 用户表 前端控制器
 * </p>
 *
 * @author yuyanan
 * @since 2019-05-06
 */
@RestController
@RequestMapping("/sys/user")
public class UserController extends SuperController {

    @Autowired
    private IUserService iUserService;

    @ApiOperation("用户列表")
    @GetMapping("/list")
    public R<IPage<User>> pageList(UserPageBo listBo) {
        Assert.notNull(ApiCode.ERROR, listBo);
        return R.ok(iUserService.pageList(listBo));
    }

    @ApiOperation(value = "用户添加")
    @PostMapping("/add")
    R<Object> add(@Validated UserAddBo addBo)
    {
        iUserService.createUser(addBo);
        return R.ok(null);
    }

//    @ApiOperation("用户批量删除")
//    @PostMapping("/dels")
//    R<Object> dels(@RequestParam("ids[]") String[] ids)
//    {
//        if (ids == null || ids.length == 0)
//        {
//            Assert.isFalse(ids == null || ids.length == 0, ApiCode.ERROR);
//        }
//        List<String> idList = Lists.newArrayList(ids);
//        try
//        {
//            sysUserService.removeByIds(idList);
//        }
//        catch (DataIntegrityViolationException e)
//        {
//            throw ApiException.Builder.warn("用户被使用中,不允许删除");
//        }
//        return ApiResult.ok(null);
//    }
//
//    @ApiOperation("用户id删除")
//    @PostMapping("/del")
//    R<Object> del(String id)
//    {
//        try
//        {
//            sysUserService.removeById(id);
//        }
//        catch (DataIntegrityViolationException e)
//        {
//            throw ApiException.Builder.warn("用户被使用中,不允许删除");
//        }
//        return ApiResult.ok(null);
//    }
//
//    @ApiOperation("修改性别")
//    @PostMapping("/sex")
//    R<Object> sex(String id, SexEnum sex)
//    {
//        sysUserService.updateSex(id, sex);
//        return ApiResult.ok(null);
//    }
//
//    @ApiOperation("帐号锁定")
//    @PostMapping("/status")
//    R<Object> status(String id, SysUserStatusEnum statusEnum)
//    {
//        if (id.equals(SysUserService.admin_id))
//        {
//            throw ApiException.Builder.warn("admin帐号是系统管理员,不允许锁定");
//        }
//        sysUserService.updateStatus(id, statusEnum);
//        return ApiResult.ok(null);
//    }

    @ApiOperation("根据id查询")
    @GetMapping("/get")
    R<User> get(String id)
    {
        Assert.notNull(ApiCode.ERROR, id);
        User user = iUserService.getUserContainRole(id);
        return R.ok(user);
    }

    @ApiOperation("用户编辑")
    @PostMapping("/edit")
    R<Object> edit(@Validated UserEditBo editBo)
    {
        iUserService.editUser(editBo);
        return R.ok(null);
    }
}
