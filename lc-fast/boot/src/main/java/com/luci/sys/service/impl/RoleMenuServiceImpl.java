package com.luci.sys.service.impl;

import com.luci.sys.entity.RoleMenu;
import com.luci.sys.mapper.RoleMenuMapper;
import com.luci.sys.service.IRoleMenuService;
import com.luci.common.supers.SuperServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 角色与菜单对应关系 服务实现类
 * </p>
 *
 * @author yuyanan
 * @since 2019-05-09
 */
@Service
public class RoleMenuServiceImpl extends SuperServiceImpl<RoleMenuMapper, RoleMenu> implements IRoleMenuService {

}
