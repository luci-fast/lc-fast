package com.luci.sys.mapper;

import com.luci.common.supers.SuperMapper;
import com.luci.sys.entity.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 用户表 Mapper 接口
 * </p>
 *
 * @author yuyanan
 * @since 2019-05-06
 */
public interface UserMapper extends SuperMapper<User> {
}
