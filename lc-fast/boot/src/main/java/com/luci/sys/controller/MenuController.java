package com.luci.sys.controller;


import com.luci.base.SuperController;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;

/**
 * <p>
 * 菜单管理 前端控制器
 * </p>
 *
 * @author yuyanan
 * @since 2019-05-09
 */
@Controller
@RequestMapping("/sys/menu")
public class MenuController extends SuperController {

}
