package com.luci.sys.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.luci.common.supers.SuperEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 用户与角色对应关系
 * </p>
 *
 * @author yuyanan
 * @since 2019-05-09
 */

@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@Data
@TableName("sys_user_role")
public class UserRole extends SuperEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 用户ID
     */
    private String userId;

    /**
     * 角色ID
     */
    private String roleId;


}
