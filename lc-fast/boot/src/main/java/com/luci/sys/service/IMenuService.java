package com.luci.sys.service;

import com.luci.sys.entity.Menu;
import com.luci.common.supers.SuperService;

/**
 * <p>
 * 菜单管理 服务类
 * </p>
 *
 * @author yuyanan
 * @since 2019-05-09
 */
public interface IMenuService extends SuperService<Menu> {

}
