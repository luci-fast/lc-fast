package com.luci.sys.model;

import com.luci.common.supers.SuperPageBo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 角色分页查询参数类
 *
 * @author yuyanan
 * @date 2018年9月11日
 */
@ApiModel("角色分页查询参数类")
@Data
public class RolePageBo extends SuperPageBo {
	
	@ApiModelProperty("角色名称")
    private String roleName;
    
}
