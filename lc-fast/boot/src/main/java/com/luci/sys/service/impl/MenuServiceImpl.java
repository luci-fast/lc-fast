package com.luci.sys.service.impl;

import com.luci.sys.entity.Menu;
import com.luci.sys.mapper.MenuMapper;
import com.luci.sys.service.IMenuService;
import com.luci.common.supers.SuperServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 菜单管理 服务实现类
 * </p>
 *
 * @author yuyanan
 * @since 2019-05-09
 */
@Service
public class MenuServiceImpl extends SuperServiceImpl<MenuMapper, Menu> implements IMenuService {

}
