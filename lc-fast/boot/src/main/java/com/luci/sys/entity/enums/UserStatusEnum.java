package com.luci.sys.entity.enums;

import com.baomidou.mybatisplus.core.enums.IEnum;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.luci.common.supers.IBindEnum;

/**
 * 用户帐号状态
 *
 * @author yuyanan
 * @date 2018年7月21日
 */
@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum UserStatusEnum implements IEnum<Integer>,
        IBindEnum {

    OPEN(1,"启用"),LOCK(2,"禁用"),;

    private Integer value;
    private String desc;

    UserStatusEnum(final int value, final String desc) {
        this.value = value;
        this.desc = desc;
    }

    UserStatusEnum(final int value) {
        this.value = value;
    }

    @Override
    public String getKey() {
        return value.toString();
    }

    @Override
    public Integer getValue() {
        return value;
    }

    public String getDesc() {
        return desc;
    }

}
