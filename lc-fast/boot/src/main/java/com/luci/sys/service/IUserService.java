package com.luci.sys.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.luci.common.supers.SuperService;
import com.luci.sys.entity.User;
import com.luci.sys.model.UserAddBo;
import com.luci.sys.model.UserEditBo;
import com.luci.sys.model.UserPageBo;

/**
 * <p>
 * 用户表 服务类
 * </p>
 *
 * @author yuyanan
 * @since 2019-05-06
 */
public interface IUserService extends SuperService<User> {

    /**
     * 分页查询
     */
    IPage<User> pageList(UserPageBo userListBo);

    /**
     * 添加
     * @param addBo
     */
    void createUser(UserAddBo addBo);

    /**
     * 获取用户包含角色
     */
    User getUserContainRole(String id);

    /**
     * 编辑
     * @param editBo
     */
    void editUser(UserEditBo editBo);

    /**
     * 登陆
     *
     * @param username 用户名
     * @param password 密码
     * @return SysUser
     */
    User login(String username, String password);
}
