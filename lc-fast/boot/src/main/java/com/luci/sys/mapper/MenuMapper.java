package com.luci.sys.mapper;

import com.luci.sys.entity.Menu;
import com.luci.common.supers.SuperMapper;

/**
 * <p>
 * 菜单管理 Mapper 接口
 * </p>
 *
 * @author yuyanan
 * @since 2019-05-09
 */
public interface MenuMapper extends SuperMapper<Menu> {

}
