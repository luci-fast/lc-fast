package com.luci.sys.service;

import com.luci.sys.entity.RoleMenu;
import com.luci.common.supers.SuperService;

/**
 * <p>
 * 角色与菜单对应关系 服务类
 * </p>
 *
 * @author yuyanan
 * @since 2019-05-09
 */
public interface IRoleMenuService extends SuperService<RoleMenu> {

}
