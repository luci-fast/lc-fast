package com.luci.sys.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.Version;
import com.luci.common.supers.SuperEntity;
import com.luci.sys.entity.enums.SexEnum;
import com.luci.sys.entity.enums.UserStatusEnum;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.time.LocalDateTime;

/**
 * <p>
 * 用户表
 * </p>
 *
 * @author yuyanan
 * @since 2019-05-06
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("sys_user")
public class User extends SuperEntity {

    private static final long serialVersionUID = 1L;

    @TableField(exist = false)
    private Role role;

    /**
     * 用户名
     */
    private String username;

    /**
     * 密码
     */
    private String password;

    /**
     * 真实姓名
     */
    private String realname;

    /**
     * 性别 1男 2女
     */
    private SexEnum sex;

    /**
     * 部门id
     */
    private String deptId;

    /**
     * email
     */
    private String email;

    /**
     * 手机号
     */
    private String mobile;

    /**
     * 头像url
     */
    private String hobby;

    /**
     * 状态 1启用 2禁用
     */
    private UserStatusEnum status;

    /**
     * 生日
     */
    private LocalDateTime birth;

    /**
     * 头像地址
     */
    private String headUrl = "http://image.baidu.com/search/detail?ct=503316480&z=undefined&tn=baiduimagedetail&ipn=d&word=%E5%B0%8F%E5%A4%B4%E5%83%8F&step_word=&ie=utf-8&in=&cl=2&lm=-1&st=undefined&hd=undefined&latest=undefined&copyright=undefined&cs=3547394372,1748543819&os=1512273966,4153715064&simid=0,0&pn=27&rn=1&di=196311986970&ln=1826&fr=&fmq=1557682338237_R&fm=&ic=undefined&s=undefined&se=&sme=&tab=0&width=undefined&height=undefined&face=undefined&is=0,0&istype=0&ist=&jit=&bdtype=0&spn=0&pi=0&gsm=0&objurl=http%3A%2F%2Fb-ssl.duitang.com%2Fuploads%2Fitem%2F201407%2F25%2F20140725214106_exjTx.thumb.224_0.jpeg&rpstart=0&rpnum=0&adpicid=0&force=undefined";

    /**
     * 省
     */
    private String province;

    /**
     * 市
     */
    private String city;

    /**
     * 县
     */
    private String district;

    /**
     * 居住地址
     */
    private String liveAddress;

    /**
     * 创建人
     */
    private String createUserId;

    /**
          * 是否删除 1是 0否
          */
    @TableLogic
    @TableField(value = "deleted")
    private Integer deleted;

    /**
     * 乐观锁-版本号
     */
    @Version
    @TableField(value = "version")
    private Integer version;
}
