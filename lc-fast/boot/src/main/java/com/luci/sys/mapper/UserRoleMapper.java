package com.luci.sys.mapper;

import com.luci.sys.entity.UserRole;
import com.luci.common.supers.SuperMapper;

/**
 * <p>
 * 用户与角色对应关系 Mapper 接口
 * </p>
 *
 * @author yuyanan
 * @since 2019-05-09
 */
public interface UserRoleMapper extends SuperMapper<UserRole> {

}
