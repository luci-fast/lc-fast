package com.luci.sys.mapper;

import com.luci.sys.entity.Role;
import com.luci.common.supers.SuperMapper;

/**
 * <p>
 * 角色 Mapper 接口
 * </p>
 *
 * @author yuyanan
 * @since 2019-05-09
 */
public interface RoleMapper extends SuperMapper<Role> {

}
