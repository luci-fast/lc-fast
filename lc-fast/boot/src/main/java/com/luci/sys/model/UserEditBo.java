/**  
 * All rights Reserved, Designed By www.tydic.com
 * @Title:  UserEditBo.java
 * @Package com.fast.admin.model.bo   
 * @Description:   
 * @author: yuyanan  
 * @date:   2018年9月17日   
 * @version V1.0 
 * @Copyright:  yuyanan
 * 
 */
package com.luci.sys.model;


import com.luci.sys.entity.enums.SexEnum;
import com.luci.sys.entity.enums.UserStatusEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**    
 *  
 * @author: yuyanan
 * @date:   2018年9月17日      
 */
@Data
@ApiModel("用户编辑入参")
public class UserEditBo implements Serializable{
	
	private static final long serialVersionUID = 1L;

	@NotBlank
    private String id;
    
    @ApiModelProperty("用户名")
    private String realname;
    
    @ApiModelProperty("性别")
    private SexEnum sex;
    
    @ApiModelProperty("邮箱")
    private String email;
    
    @ApiModelProperty("手机号")
    private String mobile;
    
    @ApiModelProperty("账号状态")
    private UserStatusEnum status;
    
    /**
     * 角色id
     */
    @ApiModelProperty("角色id")
    private String roleId;
}
