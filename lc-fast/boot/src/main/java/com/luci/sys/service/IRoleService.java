package com.luci.sys.service;

import com.luci.sys.entity.Role;
import com.luci.common.supers.SuperService;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 角色 服务类
 * </p>
 *
 * @author yuyanan
 * @since 2019-05-09
 */
public interface IRoleService extends SuperService<Role> {

    /**
     * 根据用户id查询
     * @param userId
     * @return
     */
    Role getByUserId(String userId);

    /**
     * 根据用户id集合查询
     * @param userIds
     * @return 用户id ：角色
     */
    Map<String,Role> getMapByUserIds(List<String> userIds);

    /**
     * 根据用户id集合查询
     * @param userIds
     * @return userid : SysRole
     */
    List<Role> getByUserIds(List<String> userIds);
}
