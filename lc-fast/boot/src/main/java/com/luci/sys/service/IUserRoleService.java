package com.luci.sys.service;

import com.luci.common.supers.SuperService;
import com.luci.sys.entity.UserRole;

import java.util.List;

/**
 * <p>
 * 用户与角色对应关系 服务类
 * </p>
 *
 * @author yuyanan
 * @since 2019-05-09
 */
public interface IUserRoleService extends SuperService<UserRole> {

    /**
     * 根据userId查询
     *
     * @param userId userId
     * @return UserRole
     */
    UserRole getByUserId(String userId);

    /**
     * 根据userIds查询
     *
     * @param userIds userIds
     * @return List<UserRole>
     */
    List<UserRole> getByUserIds(List<String> userIds);
}
