package com.luci.sys.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.luci.common.supers.SuperEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.time.LocalDateTime;

/**
 * <p>
 * 角色
 * </p>
 *
 * @author yuyanan
 * @since 2019-05-09
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("sys_role")
public class Role extends SuperEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 角色名称
     */
    private String roleName;

    /**
     * 角色标识
     */
    private String roleSign;

    /**
     * 备注
     */
    private String remark;

    /**
     * 创建人
     */
    private String createUserId;

    /**
     * 是否删除 1是 2否
     */
    private Boolean deleted;

    /**
     * 版本号
     */
    private Integer version;

}
