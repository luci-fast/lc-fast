package com.luci.sys.entity.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.baomidou.mybatisplus.core.enums.IEnum;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.luci.common.supers.IBindEnum;


/**
 * 用户性别
 *
 * @author yuyanan
 * @date   2018年7月21日
 */
@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum SexEnum implements IBindEnum {
    MAN(1, "男"), WOMAN(2, "女"),;

    @EnumValue
    private final int sex;
    private final String desc;

    SexEnum(Integer sex, String desc) {
        this.sex = sex;
        this.desc = desc;
    }

    @Override
    public String getKey() {
        return sex + "";
    }

    public Integer getSex() {
        return sex;
    }

    public String getDesc() {
        return desc;
    }
}
