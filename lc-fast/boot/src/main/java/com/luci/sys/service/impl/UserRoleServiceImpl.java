package com.luci.sys.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.api.Assert;
import com.luci.common.exception.ApiCode;
import com.luci.sys.entity.Role;
import com.luci.sys.entity.UserRole;
import com.luci.sys.mapper.UserRoleMapper;
import com.luci.sys.service.IUserRoleService;
import com.luci.common.supers.SuperServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.List;

/**
 * <p>
 * 用户与角色对应关系 服务实现类
 * </p>
 *
 * @author yuyanan
 * @since 2019-05-09
 */
@Service
public class UserRoleServiceImpl extends SuperServiceImpl<UserRoleMapper, UserRole> implements IUserRoleService {

    @Override
    public UserRole getByUserId(String userId) {
        QueryWrapper<UserRole> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("user_id", userId);
        return getOne(queryWrapper);
    }

    @Override
    public List<UserRole> getByUserIds(List<String> userIds) {
        Assert.isFalse(CollectionUtils.isEmpty(userIds), ApiCode.ERROR);
        return list(new QueryWrapper<UserRole>().in("user_id", userIds));
    }
}
