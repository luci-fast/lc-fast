package com.luci.sys.mapper;

import com.luci.sys.entity.RoleMenu;
import com.luci.common.supers.SuperMapper;

/**
 * <p>
 * 角色与菜单对应关系 Mapper 接口
 * </p>
 *
 * @author yuyanan
 * @since 2019-05-09
 */
public interface RoleMenuMapper extends SuperMapper<RoleMenu> {

}
