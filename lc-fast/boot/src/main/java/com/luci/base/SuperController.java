package com.luci.base;//package com.luci.fast.common.supers;

import com.baomidou.mybatisplus.extension.api.ApiController;
import com.luci.sys.entity.User;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.http.HttpServletRequest;

/**
 * web接口父类
 *
 * @author yuyanan
 * @date 2019-01-01
 */
public class SuperController extends ApiController {
    @Autowired
    protected HttpServletRequest request;

    public static final String SESSION_USER = "sysUser";

    protected void setSessionUser(User user) {
        request.getSession().setAttribute(SESSION_USER, user);
    }

    protected User getSessionUser() {
        if (request.getSession().getAttribute(SESSION_USER) == null) {
            return null;
        }
        return (User)request.getSession().getAttribute(SESSION_USER) ;
    }
}
