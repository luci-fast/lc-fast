package com.luci.base;

import com.baomidou.mybatisplus.extension.api.R;
import com.luci.sys.entity.User;
import com.luci.sys.service.IUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * admin登录
 *
 * @author yuyanan
 * @date 2018年9月11日
 */
@Api(tags = "admin-登陆")
@RestController
public class LoginController extends SuperController {

	@Autowired
	private IUserService userService;

	@ApiOperation(value = "登陆")
	@PostMapping("/admin/login")
	R<Object> login(String username, String password) {
		User user = userService.login(username, password);
		setSessionUser(user);
		return R.ok(null);
	}

}
