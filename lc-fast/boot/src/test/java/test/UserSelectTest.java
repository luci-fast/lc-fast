package test;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.luci.FastApplication;
import com.luci.sys.entity.User;
import com.luci.sys.mapper.UserMapper;
import com.luci.sys.service.IUserService;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * sys_user 查询
 *
 * @author haospzzz@163.com
 * @version 1.0
 * @date 2019-05-07
 */
@Slf4j
@RunWith(SpringRunner.class)
@EnableTransactionManagement
@SpringBootTest(classes = FastApplication.class)
public class UserSelectTest {

    @Autowired
    private IUserService userService;

    @Autowired
    private UserMapper userMapper;

    @Test
    public void selectByLike() {
        User userList = userService.getById("admin");
        log.info("user list size : {}", userList);
    }

    @Test
    public void selectList() {
        List<User> userList = userMapper.selectList(Wrappers.<User>query().gt("sex", 1));
        log.info("user list size : {}", userList.size());
    }

}
