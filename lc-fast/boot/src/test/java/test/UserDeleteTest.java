package test;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.luci.FastApplication;
import com.luci.sys.entity.User;
import com.luci.sys.service.IUserService;
import java.util.HashMap;
import java.util.Map;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.EnableTransactionManagement;


@Slf4j
@RunWith(SpringRunner.class)
@EnableTransactionManagement
@SpringBootTest(classes = FastApplication.class)
public class UserDeleteTest {

    @Autowired
    private IUserService userService;

    //根据字段删除
    @Rollback(false)
    @Test
    public void delete() {
        Map<String,Object> map  = new HashMap<>();
        map.put("password", "aaaaa");
        userService.removeByMap(map);
    }

    //根据条件构造器删除
    @Rollback(false)
    @Test
    public void delete2() {
        Map<String,Object> map  = new HashMap<>();
        map.put("password", "aaaaa");
        userService.remove(new QueryWrapper<User>().eq("password", "aaaaa"));
    }

}