package test;


import com.luci.FastApplication;
import com.luci.sys.entity.User;
import com.luci.sys.service.IUserService;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.EnableTransactionManagement;


@Slf4j
@RunWith(SpringRunner.class)
@EnableTransactionManagement
@SpringBootTest(classes = FastApplication.class)
public class UserAddTest {

    @Autowired
    private IUserService userService;

    //添加
    @Rollback(false)
    @Test
    public void save() {
        User user = new User();
        user.setUsername("yuyanan" + ThreadLocalRandom.current().nextInt(100000));
        userService.save(user);
    }

    //批量添加
    @Rollback(false)
    @Test
    public void batchSave() {
        List<User> batchUser =  IntStream.range(1,5).mapToObj(x -> {
            User u = new User();
            u.setUsername("batch" + ThreadLocalRandom.current().nextInt(100000));
            return u;
        }).collect(Collectors.toList());
        userService.saveBatch(batchUser);
    }

    //批量添加 每次提交数量
    @Rollback(false)
    @Test
    public void batchSaveLimit() {
        List<User> batchUser =  IntStream.range(1,100).mapToObj(x -> {
            User u = new User();
            u.setUsername("batch" + ThreadLocalRandom.current().nextInt(100000));
            return u;
        }).collect(Collectors.toList());
        userService.saveBatch(batchUser, 10);
    }



    
    
    
    
}
