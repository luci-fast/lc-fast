/**  
 * All rights Reserved, Designed By www.tydic.com
 * @Title:  SuperServiceImpl.java   
 * @Package com.fast.common.supers   
 * @Description:    TODO(用一句话描述该文件做什么)   
 * @author yuyanan
 * @date   2018年7月21日
 * @version V1.0 
 * @Copyright:  yuyanan
 * 
 */
package com.luci.common.supers;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.google.common.collect.Maps;
import org.springframework.util.CollectionUtils;

import java.util.List;
import java.util.Map;

/**
 * 
 * @author yuyanan
 * @date 2018年7月21日
 */
public abstract class SuperServiceImpl<M extends SuperMapper<T>, T> extends
		ServiceImpl<M, T> implements SuperService<T> {

	/**
	 * 根据主键获取map
	 * @return Map<String, T>
	 */
	@Override
	public Map<String, T> getMapByIds(List<String> ids){
		if (CollectionUtils.isEmpty(ids)) {
			return Maps.newHashMap();
		}
		return (Map<String, T>) getMap(new QueryWrapper<T>().in("id", ids));
	}
}
