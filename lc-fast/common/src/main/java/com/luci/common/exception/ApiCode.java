/**
 * All rights Reserved, Designed By www.tydic.com
 *
 * @Title: ApiCode.java
 * @Package com.fast.common.api
 * @Description:
 * @author yuyanan
 * @date 2018年9月11日
 * @version V1.0
 * @Copyright: yuyanan
 */
package com.luci.common.exception;


import com.baomidou.mybatisplus.extension.api.IErrorCode;

/**
 * api 返回码
 *
 * @author yuyanan
 * @date 2018年9月11日
 */
public enum ApiCode implements IErrorCode {

    /** 错误，系统错误类信息 */
    ERROR(10, "bug，请联系管理员"),
    WARN(20, "提示信息"),
    LOGIN_INVALID(101, "登录超时,请重新登陆"),
    USERNAME_EXIST(101, "用户名已存在"),

    ;

    private final long code;
    private final String msg;

    ApiCode(final long code, final String msg) {
        this.code = code;
        this.msg = msg;
    }


    @Override
    public long getCode() {
        return code;
    }

    @Override
    public String getMsg() {
        return msg;
    }

    @Override
    public String toString() {
        return String.format(" ErrorCode:{code=%s, msg=%s} ", code, msg);
    }
}
