package com.luci.common.supers;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 实体父类
 *
 * @author yuyanan
 * @since 2018-07-13
 */
@Data
public class SuperEntity implements Serializable {

    /**
     * 主键 uuid
     */
    @TableId(type = IdType.UUID)
    private String id;

    /**
     * 创建时间
     */
    @TableField(value = "create_time", fill = FieldFill.INSERT)
    private Date createTime;

    /**
     * 更新时间
     */
    @TableField(value = "update_time", fill = FieldFill.UPDATE)
    private Date updateTime;

}
