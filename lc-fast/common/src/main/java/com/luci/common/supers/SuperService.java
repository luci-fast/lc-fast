/**
 * All rights Reserved, Designed By www.tydic.com
 *
 * @Title: SuperService.java
 * @Package com.fast.common.supers
 * @Description: TODO(用一句话描述该文件做什么)
 * @author yuyanan
 * @date 2018年7月21日
 * @version V1.0
 * @Copyright: yuyanan
 */
package com.luci.common.supers;

import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;
import java.util.Map;

/**
 * 业务接口父类
 *
 * @author yuyanan
 * @date 2018年7月21日
 */
public interface SuperService<T> extends IService<T> {

    /**
     * 根据主键获取map
     *
     * @return Map<String, T>
     */
    Map<String, T> getMapByIds(List<String> ids);
}
